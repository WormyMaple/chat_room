import asyncio
from websockets import serve
from websockets import exceptions
import json

sockets = []
trace_back = []
trace_back_length = 100

users = {}

def load_users():
    global users

    with open("usernames.json") as json_file:
        users = json.load(json_file)
    print(users)

def add_user(username, password):
    if username in users:
        return "E: USER ALREADY EXISTS"
    else: 
        users[username] = password
        save_users()
        return True

def save_users():
    with open("usernames.json", "w") as json_file:
        json.dump(users, json_file, indent = 1)

def check_creds(username, password):
    if username in users:
        if users[username] == password:
            return True
        else:
            return "E: MISMATCHED CREDENTIALS"
    else:
        return "E: USER DOES NOT EXIST"

async def connect(websocket):
    print("New connection from: " + str(websocket))

    logged_in = False

    while not logged_in:
        credentials = await websocket.recv() 
        info = credentials.split(", ")

        login_type = info[0]
        username = info[1]
        password = info[2]

        if (login_type == "CREDS"):
            check = check_creds(username, password)
        else:
            check = add_user(username, password)

        if check == True:
            logged_in = True
            await websocket.send("CONNECT")
            print(username + " connected")
        elif check == "E: MISMATCHED CREDENTIALS":
            await websocket.send("E: Credentials do not match")
        elif check == "E: USER DOES NOT EXIST":
            await websocket.send("E: User does not exists, check spelling or create a new account")
        else:
            await websocket.send("E: Username already exists, choose another")

    # serve traceback
    for message in trace_back:
        await websocket.send(message)

    sockets.append(websocket)

    async for message in websocket:
        if (message == "EXIT"):
            sockets.remove(websocket)
            break

        print(message)
        await distribute(message)
        
async def distribute(message):
    # traceback
    if (message[0] == 'M'):
        trace_back.append(message)
        if len(trace_back) > trace_back_length:
            trace_back.pop(0)

    errored_sockets = []

    for socket in sockets:
        try:
            await socket.send(message)
        except Exception as e:
            print("sending to " + str(socket) + " caused an error, removing")
            errored_sockets.append(socket)
    
    # remove errored sockets from main list
    for socket in errored_sockets:
        sockets.remove(socket)

load_users()
ip = input("Served IP: ")

async def main():
    async with serve(connect, ip, 8765):
        await asyncio.Future()  # run forever

asyncio.run(main())
